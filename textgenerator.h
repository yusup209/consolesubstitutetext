#ifndef TEXTGENERATOR_H
#define TEXTGENERATOR_H

#include <QObject>
#include <QFile>
#include <QIODevice>
#include <QDebug>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QRegularExpression>
#include <regex>

class TextGenerator : public QObject
{
    Q_OBJECT
public:
    explicit TextGenerator(QObject *parent = nullptr);

public slots:
    QString Start(QString msg);

private slots:
    QString TextSubstitution(QString msg);
    QStringList MatchWordWithRegex(std::regex theRegex, QString msg);

signals:

};

#endif // TEXRGEMERATOR_H
