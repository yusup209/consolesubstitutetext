#include <QCoreApplication>
#include <thread>
#include <iostream>
#include <ctype.h>
#include <regex>
#include "textgenerator.h"

void PrintMatches(std::string theString, std::regex theRegex){
    std::smatch match;
    std::cout << std::boolalpha;

    while (std::regex_search(theString, match, theRegex)){
//        std::cout << "is there a match : " << match.ready() << "\n";
//        std::cout << "are there no matches : " << match.empty() << "\n";
//        std::cout << "number of matches : " << match.size() << "\n";
//        std::cout << "match position : " << match.position() << "\n";
        std::cout << match.str(1) << "\n";

        theString = match.suffix().str();
        std::cout << "\n";
    }
}

void PrintMatches2(std::string str, std::regex reg){
    std::sregex_iterator currentMatch (str.begin(), str.end(), reg);
    std::sregex_iterator lastMatch;

    while (currentMatch != lastMatch){
        std::smatch match = *currentMatch;
        std::cout << match.str() << "\n";
        currentMatch++;
    }

    std::cout << std::endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::string str = "hello ape from apex and apes apes sekali";
    std::regex regEx ("(ape[^ ]?)");
    PrintMatches(str, regEx);

    std::string str2 = "you've picked the pickle and pickless";
    std::regex regEx2("(pick([^ ]+)?)");
    PrintMatches2(str2, regEx2);

//    std::string str3 = "halo assalamualaikum dan selamat datang di kopi gelud cafe hehehe";
//    std::regex regEx3("([aegijosz]([^ .|^ ]+)?)");
//    PrintMatches2(str3, regEx3);

    std::string str4 = "Cat pat fat cat mat rat";
    std::regex regEx4("([pfcmr]at)");
    std::regex regEx5("([Cpfcmr]at)");
    PrintMatches2(str4, regEx4);
    PrintMatches2(str4, regEx5);


    //section replace matches
    std::regex regEx6("([Cr]at)");
    std::string owlFood = std::regex_replace(str4, regEx6, "Owl");
    std::cout << owlFood << "\n";

    //find word with dot and exclude space
    std::string str7 = "T.N.I B.I.N POLRI";
    std::regex regEx7 ("([^ ]\..\..\.)");
    PrintMatches2(str7, regEx7);

    //find multiline string
    std::string str8 = "This is\na multiline string\nmade by woods";
    std::regex regEx8 ("\n");
    std::string noLBString = std::regex_replace(str8, regEx8, " ");
    std::cout << noLBString << "\n";

    //detect num
    std::string str9 = "123 12345 1234567 123456789";
    std::regex regEx9 ("\\d{5,7}");
    PrintMatches2(str9, regEx9);

    // \w [a-zA-Z0-9] detect word
    // \W [^a-zA-Z0-9] detect non word
    std::string str10 = "021-000-0000";
    std::regex regEx10 ("\\w{3}-\\w{3}-\\w{4}");
    PrintMatches2(str10, regEx10);

    std::string str11 = "Si Mawang";
    std::regex regEx11 ("\\w{2,20}\\s\\w{2,20}"); //detect 2 word that separated by space/whitespace. range from 2 - 20
    PrintMatches2(str11, regEx11);

    std::string str12 = "a an ape apes bug";
    std::regex regEx12 ("a[a-z]+");
    PrintMatches2(str12, regEx12);

    std::string str13 = "hambaallah213@contoso.com";
    std::regex regEx13 ("\\w{1,20}[a-zA-Z]\\d{1,5}?\\..?\\_?\%?\\+?\\-?@\\w{2,20}[a-zA-Z]\\d{1,5}?\\..?\\-?\\..\\w{2,3}[a-zA-Z]"); //nanti kalo tes di web pake single backslash sadja

    PrintMatches(str13, regEx13);


    //coba text generator
    TextGenerator textGen;
    QString teksSumber = "Hallo selamat siang jangan lupa kunjungi link di bawah ini yaa https://google.com https://facebook.com";
    QString teksHasil = textGen.Start(teksSumber);
    qDebug() << "hasil dari teks generator : " << teksHasil << "\n";

    return a.exec();
}
