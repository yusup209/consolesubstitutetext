#include "textgenerator.h"

TextGenerator::TextGenerator(QObject *parent) : QObject(parent)
{
    std::srand(time(NULL));
}

QString TextGenerator::Start(QString msg)
{
    qDebug() <<"bagian read json";
    //read json file and decode it
    QFile search_array("F:/QtProjects/consoleSubstituteText/data.json");
    if (!search_array.open(QIODevice::ReadOnly|QIODevice::Text)) qFatal("gagal membuka file data.json\n");
    QByteArray byteArray = search_array.readAll();
    search_array.close();

    QJsonParseError err;
    qDebug() << "bagian parsing json kedalam objek";
    QJsonDocument doc = QJsonDocument::fromJson(byteArray, &err);
    if (doc.isNull()) qFatal("Fail to parse json");
    QJsonObject jsonObj = doc.object(); //as dec


    //explode string into array of words and count it
    qDebug() << "split pesan dengan delimiter whitespace alias `spasi`";
    QStringList listOfWords = msg.split(QRegularExpression("\\s+"), QString::SkipEmptyParts);
    //    int wordsCount = listOfWords.count();

    QStringList kataFix;

    qDebug() << "mau masuk ke pengulangan objek listOfWords";
    qDebug() << "jumlah kata listOfWords : " << QString::number(listOfWords.count());
    for (QString &str : listOfWords){
        QJsonObject existKey = jsonObj.value(str).toObject();
        if (str.length() <= 500 || str.length() >= 1000){
            kataFix.append(str);
        } else if(existKey.count() > 0){
            QString hasil = existKey.value("sinonim").toString();
            QStringList sinonim;
            int sinonim_count = sinonim.count();

            for (QString a : hasil){
                sinonim.append(a);
            }

            kataFix.append(sinonim[rand() % sinonim_count]);
        } else {
            kataFix.append(str);
        }
    }

    qDebug() << "berhasil melewati pegulangan listOfWords";
    qDebug() << "jumlah objek kataFix : " + QString::number(kataFix.count());

    QString hayo = "";

    for (QString &hasil : kataFix){
//        qDebug() << "hayoooo";
//        qDebug() << "hasil : " + hasil;
        hayo += TextSubstitution(hasil+" ");

    }
    return hayo;
}

QString TextGenerator::TextSubstitution(QString msg)
{
    qDebug() << "masuk ke fungsi substitusi teksnya";
    //substitute urlnya
    QString penggantiURLsementara = "@@@@";
    std::regex regEx ("#[-a-zA-Z0-9@:%_\\+.~\\#?&//=]{2,256}\\.[a-z]{2,4}\b(\\/[-a-zA-Z0-9@:%_\\+.~\\#?&//=]*)?#si");
    QStringList aa = MatchWordWithRegex(regEx, msg);

    //substitute url to bbcode
    QStringList text;
//    int n = 0;

    qDebug() << "mau masuk ke pegulangan dari objek aa (QStringList) yang isinya match regex dengan url";
    qDebug() << "jumlah objek aa : " + QString::number(aa.count());
    for (QString item : aa){
        qDebug() << "mamah dan aa beraksi";
        //ori php
        // $text[] = str_replace($url[0], $penggantiURLsementara, $msg);
        text.append(aa[0].replace(aa[0], penggantiURLsementara));
    }

    //substitute the message
    QStringList sub_a; sub_a << "4" << "A" << "α";
    QStringList sub_e; sub_e << "e" << "E" << "ę";
    QStringList sub_g; sub_g << "9" << "G" << "Ģ";
    QStringList sub_i; sub_i << "i" << "I" << "į";
    QStringList sub_j; sub_j << "7" << "J" << "j";
    QStringList sub_o; sub_o << "o" << "O" << "Ő";
    QStringList sub_s; sub_s << "5" << "S" << "ς";
    QStringList sub_z; sub_z << "z" << "Z" << "2";
    QStringList find; find << "a"<<"e"<<"g"<<"i"<<"j"<<"o"<<"s"<<"z"<<"A"<<"E"<<"G"<<"I"<<"J"<<"O"<<"S"<<"Z";
    QRegularExpression regFind("[a-A|e-E|g-G|i-I|j-J|o-O|s-S|z-Z]");

    QStringList replace;
    for (int i=0;i<2;i++){
        replace.append(sub_a[rand() % 3]);
        replace.append(sub_e[rand() % 3]);
        replace.append(sub_g[rand() % 3]);
        replace.append(sub_i[rand() % 3]);
        replace.append(sub_j[rand() % 3]);
        replace.append(sub_o[rand() % 3]);
        replace.append(sub_s[rand() % 3]);
        replace.append(sub_z[rand() % 3]);
    }

    int rand_num = rand() % 5;
    QString txt_mentah;

    //    switch(rand_num){
    //    case 0:
//            QString txt_header = text[0].mid(0, 50);
//            QString txt_body = text[0].mid(50, 100);
//            QString txt_footer = text[0].mid(100, text[0].count());
//            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
    //        break;

    //    case 1:
    //        //do something
    //        break;
    //    }

    switch (rand_num) {
        case 0:
        {
            QString txt_header = text[0].mid(0, 50);
            QString txt_body = text[0].mid(50, 100);
            QString txt_footer = text[0].mid(100, text[0].count());
            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
            break;
        }
        case 1:
        {
            QString txt_header = text[0].mid(0, 100);
            QString txt_body = text[0].mid(100, 200);
            QString txt_footer = text[0].mid(200, text[0].count());
            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
            break;
        }
        case 2:
        {
            QString txt_header = text[0].mid(0, 150);
            QString txt_body = text[0].mid(150, 300);
            QString txt_footer = text[0].mid(300, text[0].count());
            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
            break;
        }
        case 3:
        {
            QString txt_header = text[0].mid(0, 200);
            QString txt_body = text[0].mid(200, 400);
            QString txt_footer = text[0].mid(400, text[0].count());
            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
            break;
        }
        case 4:
        {
            QString txt_header = text[0].mid(0, 300);
            QString txt_body = text[0].mid(300, 500);
            QString txt_footer = text[0].mid(500, text[0].count());
            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
            break;
        }
        default:
        {
            QString txt_header = text[0].mid(0, 80);
            QString txt_body = text[0].mid(80, 200);
            QString txt_footer = text[0].mid(200, text[0].count());
            txt_mentah = txt_header.replace(regFind, txt_body).replace(regFind, txt_footer);
            break;
        }
    }

//    int lptr = 0;
    int txtMentah_count = txt_mentah.count();
    for (int i = 0; i < txtMentah_count; i++){

    }

    return txt_mentah;
}

QStringList TextGenerator::MatchWordWithRegex(std::regex theRegex, QString msg)
{
    QStringList strMatches;
    std::string theMsg =msg.toStdString();
    std::sregex_iterator currentMatch (theMsg.begin(), theMsg.end(), theRegex);
    std::sregex_iterator lastMatch;

    while (currentMatch != lastMatch){
        std::smatch match = *currentMatch;
        strMatches.append(QString::fromStdString(match.str()));
        currentMatch++;
    }

    return strMatches;
}
